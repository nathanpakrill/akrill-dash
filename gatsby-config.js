module.exports = {
    siteMetadata: {
        title: `Dashboard`,
        description: ``,
        author: ``,
        links: [
            {
                name: 'UG',
                url: 'https://ultimate-guitar.com',
                icon: 'fas fa-guitar'
            },
            {
                name: 'SongSelect',
                url: 'https://songselect.ccli.com',
                icon: 'fas fa-music'
            },
            {
                name: 'Gmail',
                url: 'https://mail.google.com',
                icon: 'fas fa-envelope'
            },
            {
                name: "BitBucket",
                url: 'https://bitbucket.org',
                icon: 'fab fa-bitbucket'
            },
            {
                name: "Netlify",
                url: 'https://app.netlify.com',
                icon: 'fas fa-server'
            },
            {
                name: "Bible Gateway",
                url: "https://biblegateway.com",
                icon: "fas fa-bible"
            },
            {
                name: "StackEdit",
                url: "https://stackedit.io/",
                icon: "fab fa-markdown"
            },
            {
                name: "Reddit",
                url: 'https://reddit.com',
                icon: 'fab fa-reddit'
            },
        ]
    },
    /* Your site config here */
    plugins: [
        `gatsby-plugin-react-helmet`,
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `images`,
                path: `${__dirname}/src/images`,
            },
        },
        `gatsby-transformer-sharp`,
        `gatsby-plugin-sharp`,
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                name: `akrill-dash`,
                short_name: `akrill-dash`,
                start_url: `/`,
                background_color: `#000`,
                theme_color: `#000`,
                display: `minimal-ui`,
                icon: `src/images/n-icon.png`, // This path is relative to the root of the site.
            },
        },
        `gatsby-plugin-offline`,
        `gatsby-optional-chaining`
    ],
}