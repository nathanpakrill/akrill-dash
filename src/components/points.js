import React from 'react'
import * as firebase from 'firebase'
import Counter from './counter'
import PointsStyles from '../styles/points.module.css'
require("firebase/firestore")

class Points extends React.Component {
    constructor(props) {
        super(props)

        const firebaseConfig = {
            apiKey: "AIzaSyAaxLfDi26WQ6s7bBBvm9izuqsrVgbEPCw",
            authDomain: "akrill-dash.firebaseapp.com",
            databaseURL: "https://akrill-dash.firebaseio.com",
            projectId: "akrill-dash",
            storageBucket: "akrill-dash.appspot.com",
            messagingSenderId: "677994874453",
            appId: "1:677994874453:web:a509a43eefaab68571b6dc"
        };

        if (!firebase.apps.length) {
            firebase.initializeApp(firebaseConfig);
        } else {
            firebase.app();
        }

        this.db = firebase.firestore();
    
        this.state = {
            points: []
        }

        this.db.collection('points').onSnapshot(s => {
            let pointsItems = []
            s.docs.forEach(d => {
                let points = {
                    id: d.id,
                    ...d.data()
                }
                pointsItems.push(points)
            })
            this.setState({
                points: pointsItems
            })
        }, e => {
            console.error(e)
        })
    }

    updateDb(points) {
        points.forEach(l => {
            this.db.collection('points').doc(l.id).set(l)
        })
    }

    incrementPoints(id) {
        let points = this.state.points
        points.forEach(point => {
            if (point.id === id) {
                point.Points += 1
            }
        })
        this.updateDb(points)
        this.setState({
            points: points
        })
    }

    render() {
        return (
            <article className={PointsStyles.points}>
                {this.state.points.map(pointItem => {
                    return (
                        <p>
                            {pointItem.id}: &nbsp;
                            <button style={{
                                backgroundColor: 'transparent',
                                color: 'white'
                            }} onClick={() => this.incrementPoints(pointItem.id)}>{pointItem.Points}</button>
                        </p>
                    )
                })}
            </article>
        )
    }
}

export default Points