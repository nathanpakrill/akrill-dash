import React from 'react'

class Time extends React.Component {
    constructor() {
        super();

        this.state = {
            time: '',
            date: ''
        }

        requestAnimationFrame(this.updateDateTime.bind(this));
    }

    updateDateTime() {
        let now = new Date();
        let time = {hours: now.getHours(), minutes: now.getMinutes(), seconds: now.getSeconds()};
        let date = {
            day: now.getUTCDate(),
            month: Intl.DateTimeFormat('en-GB', {
                month: 'long'
            }).format(now),
            year: now.getFullYear()
        }
        this.setState({
            time: time,
            date: date
        });
        requestAnimationFrame(this.updateDateTime.bind(this));
    }

    render() {
        return (
            <article style={{
                gridArea: 'time',
                transform: 'rotateY(calc((180deg * var(--x)))) rotateX(calc((-180deg * var(--y)))) translate(calc(100% * (1.1 * var(--x))),calc(100% * (1.1 * var(--y))))',
                willChange: 'transform',
            }}>
                <h2>{this.state.time.hours}:{this.state.time.minutes < 10 ? `0${this.state.time.minutes}` : this.state.time.minutes} <small>{this.state.time.seconds}</small></h2>
                <p><span style={{fontSize: '1.5em'}}>{this.state.date.day}</span> {this.state.date.month} {this.state.date.year}</p>
            </article>
        )
    }
}

export default Time