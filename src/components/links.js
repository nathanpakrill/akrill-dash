import React from 'react'
import {graphql, useStaticQuery} from 'gatsby'
import LinksStyles from '../styles/links.module.css'

const Links = () => {
        const {site: {siteMetadata: {links}}} = useStaticQuery(graphql`
        query siteLinks {
          site {
            siteMetadata {
              links {
                name
                url
                icon
              }
            }
          }
        }
      `)

    return (
        <div style={{
            gridArea: 'links',
            transform: 'rotateY(calc((180deg * var(--x)))) rotateX(calc((-180deg * var(--y)))) translate(calc(100% * (1.1 * var(--x))),calc(100% * (1.1 * var(--y))))',
            willChange: 'transform',
        }}>
            <ul className={LinksStyles.links}>
                {links && links.map((link,index) => {
                    return (
                        <li key={index} className={LinksStyles.link}>
                            <a href={link.url} target='_blank'>
                                <p><i className={link.icon}></i>
                                </p>
                            </a>
                        </li>
                    )
                })}
            </ul>
        </div>
    )
}

export default Links