import React from 'react'

class Counter extends React.Component {
    constructor(props) {
        super(props)
        this.updateCounter = props.onCounterUpdate;
        this.state = {
            number: props?.number,
            id: props?.id
        }
    }
    increment() {
        this.setState({
            number: this.state.number + 1
        })
    }
    componentDidUpdate(prevProps) {
        if (this.state.number === prevProps.number) {
            return
        }
        this.updateCounter(this.state.id,this.state.number)
    }
    render () {
        return (
            <span onClick={this.increment.bind(this)}>{this.state.number}</span>
        )
    }
}

export default Counter