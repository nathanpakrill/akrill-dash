import React from 'react'

const apiKey = '0947e6548cabb95c1033689ce2fc4445';


class Weather extends React.Component {
    constructor() {
        super()

        this.state = {
            weather: '',
            temperature: ''
        }
    }

    fetchWeather() {
        fetch(`https://api.openweathermap.org/data/2.5/weather?q=Bournemouth&appid=${apiKey}&units=metric`).then(r => r.json()).then(data => {
            let weather = data;
            this.setState({
                weather: weather?.weather[0]?.description,
                temperature: weather?.main?.temp
            });
        }).catch(e => console.error(e));
        setInterval(this.fetchWeather.bind(this), 36000000);
    }

    componentDidMount() {
        this.fetchWeather()
    }

    render() {
        return (
            <article style={{
                transform: 'rotateY(calc((180deg * var(--x)))) rotateX(calc((-180deg * var(--y)))) translate(calc(100% * (1.1 * var(--x))),calc(100% * (1.1 * var(--y))))',
                willChange: 'transform',
            }}>
                <p>{this.state.temperature}<br></br>{this.state.weather}</p>
            </article>
        )
    }
}

export default Weather