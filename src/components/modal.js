import React from 'react';
import ModalStyles from '../styles/modal.module.css'

class Modal extends React.Component {
    constructor(props) {
        super(props);
        this.children = props.children;
        this.state = {
            display: false
        };
    }

    show() {
        this.setState({
            display: true
        })
    }

    hide() {
        this.setState({
            display: false
        })
    }

    render() {
        return (
            <div className={ModalStyles.modal} style={{
                display: `${this.state.display ? 'flex' : 'none'}`,
            }}>
                <div onClick={this.hide.bind(this)} role={'hide modal'}></div>
                <article>
                    {this.children}
                </article>
            </div>
        )
    }
}

export default Modal