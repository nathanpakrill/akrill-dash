import React from "react"
import * as firebase from 'firebase';
import TodoStyles from '../styles/todo.module.css'
require("firebase/firestore");

class Todo extends React.Component {
    constructor() {
        super();
        const firebaseConfig = {
            apiKey: "AIzaSyAaxLfDi26WQ6s7bBBvm9izuqsrVgbEPCw",
            authDomain: "akrill-dash.firebaseapp.com",
            databaseURL: "https://akrill-dash.firebaseio.com",
            projectId: "akrill-dash",
            storageBucket: "akrill-dash.appspot.com",
            messagingSenderId: "677994874453",
            appId: "1:677994874453:web:a509a43eefaab68571b6dc"
        };
    
        if (!firebase.apps.length) {
            firebase.initializeApp(firebaseConfig);
        } else {
            firebase.app();
        }
    
        this.db = firebase.firestore();
    
        this.state = {
            lists: [],
            currentIndex: 0,
            isScrolling: null,
            scroll:false
        }
    
        this.db.collection('lists').onSnapshot(s => {
            let lists = [];
            s.docs.forEach(d => {
                let list = {
                    id: d.id,
                    ...d.data()
                }
                list.items.reverse()
                lists.push(list)
            });
            this.setState({
                lists: lists
            });
        }, e => {
            console.error(e);
        });
    }

    updateDb() {
        this.state.lists.forEach(l => {
            this.db.collection('lists').doc(l.id).set(l);
        });
    }

    addList() {
        if (!this.newListName.value || this.newListName.value === '') {
            return;
        }
        let lists = this.state.lists;
        lists.push({
            id: this.newListName.value.replace(/\s/g,''),
            title: this.newListName.value,
            items: []
        });
        this.newListName.value = '';
        this.updateDb();
    }

    deleteList(list) {
        let lists = this.state.lists;
        this.db.collection('lists').doc(list.id).delete()
        lists.splice(lists.indexOf(list),1);
        this.setState({
            lists: lists
        })
        this.updateDb();
    }

    render() {
        return (
            <div className='list-container' style={{
                gridArea: 'todo',
                display: 'flex',
                flexFlow: 'column nowrap',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                transform: 'rotateY(calc((180deg * var(--x)))) rotateX(calc((-180deg * var(--y)))) translate(calc(100% * (1.1 * var(--x))),calc(100% * (1.1 * var(--y))))',
                willChange: 'transform',
            }}>
                <h2>Todos</h2>
                <div className={TodoStyles.listHolder} onScroll={(e) => {
                    window.clearTimeout(this.state.isScrolling);
                    this.setState({
                        isScrolling: window.setTimeout(() => {
                            let scrollPercent = e.target.scrollLeft / e.target.scrollWidth;
                            let index = Math.round(scrollPercent * this.state.lists.length);
                            this.setState({
                                currentIndex: index,
                                scroll: false
                            })
                        }, 100),
                        scroll: true
                    })
                }}>
                    {
                        this.state.lists.map((list,index) => {
                            return (
                                <ul className={TodoStyles.todo} key={index}>
                                    <h2>{list && list.title}
                                        <button id={`${index}Delete`} className='delete-list' onClick={() => {this.deleteList(list)}}>
                                            <i className='fas fa-trash'></i>
                                        </button>
                                    </h2>
                                    <div>
                                        {list.items && list.items.map((item,i) => {
                                            return (
                                                <li key={i} className={(item.Ticked ? TodoStyles.ticked : '')} onClick={() => {
                                                    let lists = this.state.lists;
                                                    lists[index].items[i].Ticked = !lists[index].items[i].Ticked;
                                                    this.setState({
                                                        lists: lists
                                                    })
                                                    this.updateDb()
                                                }}>
                                                    {item.Label}
                                                </li>
                                            )
                                        })}
                                    </div>
                                    <input type='text' className='new-list-item' onKeyPress={e => {
                                        if (e.key === 'Enter') {
                                            let lists = this.state.lists;
                                            if (e.currentTarget.value && e.currentTarget.value !== '') {
                                                lists[index].items.push({
                                                    Label: e.currentTarget.value,
                                                    Ticked: false
                                                })
                                                e.currentTarget.value = '';
                                            }
                                            this.setState({
                                                lists: lists
                                            })
                                            this.updateDb()
                                        }
                                    }} />
                                    <button onClick={(e) => {
                                        let lists = this.state.lists;
                                        if (e.currentTarget.previousSibling.value && e.currentTarget.previousSibling.value !== '') {
                                            lists[index].items.push({
                                                Label: e.currentTarget.previousSibling.value,
                                                Ticked: false
                                            })
                                            e.currentTarget.previousSibling.value = '';
                                        }
                                        this.setState({
                                            lists: lists
                                        })
                                        this.updateDb()
                                    }}>Add Item</button>
                                </ul>
                            )
                        })
                    }
                </div>
                <ul className={TodoStyles.dots}>
                    {this.state.lists.map((list, index) => {
                        return (
                            <li key={index} style={{
                                width: '0.5em',
                                height: '0.5em',
                                borderRadius: 999,
                                borderColor: 'white',
                                borderWidth: 2,
                                borderStyle: 'solid',
                                backgroundColor: this.state.scroll ? 'transparent' : this.state.currentIndex === index ? 'white': 'transparent'
                            }}></li>
                        )
                    })}
                </ul>
                <input id='newListInput' ref={n => {this.newListName = n}} onKeyPress={e => {
                    if (e.key === 'Enter') {
                        this.addList();
                    }
                }} />
                <button id='addListButton' className={TodoStyles.addListButton} onClick={this.addList.bind(this)}>Add List</button>
            </div>
        )
    }

}

export default Todo