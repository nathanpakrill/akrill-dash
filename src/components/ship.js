import React from 'react'
import {Scene, PerspectiveCamera, WebGLRenderer,DirectionalLight,Vector3, AmbientLight,Group} from 'three'
import {GLTFLoader} from '../helpers/GLTFLoader'
import {OrbitControls} from '../helpers/OrbitControls'
class Ship extends React.Component {
    constructor(props) {
        super(props)

        this.sceneState = 'xwing'
        this.state = {
            loading: true
        }
    }

    componentDidMount() {
        if (typeof window === 'undefined' || matchMedia('(hover:none)').matches) {
            return;
        }
        this.scene = new Scene()
        this.camera = new PerspectiveCamera(75, this.container.offsetWidth / this.container.offsetHeight, 0.1, 1000)
        this.renderer = new WebGLRenderer({
            alpha: true
        })
        this.light = new Group()
        this.light.add(new DirectionalLight(0xababab,2))
        this.light.add(new AmbientLight(0xababab,1)) // soft white light
        this.scene.add(this.light)
        this.renderer.setClearColor(0x000000,0)
        this.renderer.setSize(this.container.offsetWidth, this.container.offsetHeight)
        this.container.appendChild(this.renderer.domElement)
        this.camera.position.z = 10
        this.controls = new OrbitControls(this.camera, this.renderer.domElement)
        this.controls.autoRotate = true
        this.loader = new GLTFLoader()
        // this.loader.load('/saber.glb', gltf => {
        //     this.saber = gltf.scene
        //     this.saber.scale.set(0.2,0.2,0.2)
        // })
        this.loader.load('/xwing.glb', gltf => {
            this.xwing = gltf.scene
            this.xwing.scale.set(0.5,0.5,0.5)
            this.loadXwing()
        })
        this.animate()
    }

    animate() {
        this.renderer.render(this.scene,this.camera)
        this.controls.update()
        requestAnimationFrame(this.animate.bind(this))
    }

    reset() {
        return new Promise((res,rej) => {
            this.scene.clear()
            this.scene.add( this.light )
            res()
        })
    }

    loadSaber() {
        this.scene.add(this.saber)
        this.light.target = this.saber
    }

    loadXwing() {
        this.scene.add(this.xwing)
        this.light.target = this.xwing
    }

    toggleScene() {
        this.reset().then(() => {
            if (this.sceneState === 'xwing') {
                this.loadSaber()
                this.sceneState = 'saber'
            }
            if (this.sceneState === 'saber') {
                this.loadXwing()
                this.sceneState = 'xwing'
            }
        })
    }

    render() {
        return (
            <div ref={n => this.container = n} style={{
                gridArea: 'ship',
                width:'100%',
                position: 'relative'
            }}>
                <>{this.state.loading ? '' : <button onClick={this.toggleScene.bind(this)}
                    style={{
                        position: 'absolute',
                        bottom: '1em',
                        left: '1em'
                    }}
                >Switch</button>}</>
            </div>
        )
    }
}

export default Ship