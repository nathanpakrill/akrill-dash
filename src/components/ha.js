import React from 'react'

const HA = () => {
    return (
        <div style={{
            gridArea: 'ship',
            width:'100%',
            position: 'relative',
            padding: 0
        }}>
            <iframe src='http://home.akrill.net:8123' loading='lazy' style={{
                border: 'none',
                borderRadius: '0.5em',
                width: '100%',
                height: '100%',
                objectFit: 'cover'
            }} />
        </div>
    )
}
export default HA