const endpoint = 'http://www.musicforprogramming.net/rss.php';
const proxy = 'https://cors-anywhere.herokuapp.com/';

function playRandom(urls) {
    let track = urls[Math.floor(Math.random() * urls.length)];
    let audio = new Audio(track);
    audio.play();
}

function init() {
    let urls = [];
    fetch(`${proxy ? proxy : ''}` + endpoint, {
        method: 'GET',
        mode: 'cors',
        headers: {
            'Access-Control-Allow-Origin':'*',
        }
    })
    .then(response => response.text(), e => {console.error(e);})
    .then(str => new window.DOMParser().parseFromString(str, "text/xml"), e => {console.error(e);})
    .then(data => {
        let tracks = [].slice.call(data.querySelectorAll('item'));
        tracks.forEach(t => {
            urls.push(t.querySelector('guid').innerHTML);
        });
        playRandom(urls);
    }, e => {console.error(e);});
}

export {
    init
}