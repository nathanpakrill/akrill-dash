import * as Lists from './_lists';
import * as Time from  './_time';
import * as Search from './_search';
import * as Weather from './_weather';
import * as Nasa from './_nasa';
import * as Speech from './_speech';
import * as Links from './_links';
import * as Planet from './_planet';
import * as Music from './_music';

function init() {
    // Lists.init();
    // Time.init();
    // Search.init();
    // Weather.init();
    // // Nasa.request();
    // Speech.init();
    // Links.fetchLinks();
    // Links.initDialog();
    // Planet.init();
    // Music.init();
}

if (document.readyState == 'complete' || document.readyState == 'interactive') {
    init();
} else {
    document.addEventListener('DOMContentLoaded', init, false);
}