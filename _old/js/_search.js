function loadSearch() {
    location.href = `https://www.google.com/search?q=${search.value.replace(/\s+/g, '+')}`;
}

function init() {
    search.addEventListener('keyup', e => {
        e.preventDefault();
        if (e.keyCode === 13) {
            loadSearch();
        }
    }, false);
}

export {
    init,
    loadSearch
}