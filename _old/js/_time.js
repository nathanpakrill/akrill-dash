function init() {
    setInterval(setTime, 1000);
    setDate();
}

function setTime() {
    time.innerHTML = `${new Date().getHours()}:${new Date().getMinutes() < 10 ? '0' : ''}${new Date().getMinutes()} <small>${new Date().getSeconds() < 10 ? '0' : ''}${new Date().getSeconds()}</small>`;
}

function setDate() {
    dateBox.innerHTML = `${new Date().toLocaleString('en-us',{month:'long', year:'numeric', day:'numeric'})}`;
}

export {
    init
}