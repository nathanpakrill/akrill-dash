import * as firebase from 'firebase';
require("firebase/firestore");

const firebaseConfig = {
    apiKey: "AIzaSyAaxLfDi26WQ6s7bBBvm9izuqsrVgbEPCw",
    authDomain: "akrill-dash.firebaseapp.com",
    databaseURL: "https://akrill-dash.firebaseio.com",
    projectId: "akrill-dash",
    storageBucket: "akrill-dash.appspot.com",
    messagingSenderId: "677994874453",
    appId: "1:677994874453:web:a509a43eefaab68571b6dc"
};

let db;
let lists;

function updateListeners() {
    let buttons = [].slice.call(document.querySelectorAll('.add-item'));

    if (buttons) {
        buttons.forEach(b => {
            b.addEventListener('click', () => {
                let input = document.querySelector(`#${b.dataset.list}Input`);
                if (input.value == '') {
                    return;
                }
                lists[b.dataset.list].items.push({
                    Label: input.value,
                    Ticked: false
                });
                update();
            }, false);
        });
    }

    let items = [].slice.call(document.querySelectorAll('.list__item'));

    if (items) {
        items.forEach(i => {
            i.addEventListener('click', () => {
                i.classList.toggle('ticked');
                lists[i.dataset.list].items[i.dataset.index].Ticked = !lists[i.dataset.list].items[i.dataset.index].Ticked;
                console.log(lists);
                
                update();
            });
        });
    }

    let listDeletes = [].slice.call(document.querySelectorAll('.delete-list'));

    if (listDeletes) {
        listDeletes.forEach(d => {
            d.addEventListener('click', () => {
                lists[d.dataset.list] = null;
                update();
            });
        });
    }
}

function renderLists() {
    let html = '';
    Object.entries(lists).forEach(l => {
        let list = l[1];
        let id = l[0];
        html += `
        <ul class='list'>
            <h2>${list.title} <button id='${id}Delete' data-list='${id}' class='delete-list'><svg
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M17 5V4C17 2.89543 16.1046 2 15 2H9C7.89543 2 7 2.89543 7 4V5H4C3.44772 5 3 5.44772 3 6C3 6.55228 3.44772 7 4 7H5V18C5 19.6569 6.34315 21 8 21H16C17.6569 21 19 19.6569 19 18V7H20C20.5523 7 21 6.55228 21 6C21 5.44772 20.5523 5 20 5H17ZM15 4H9V5H15V4ZM17 7H7V18C7 18.5523 7.44772 19 8 19H16C16.5523 19 17 18.5523 17 18V7Z"
              fill="currentColor"
            />
            <path d="M9 9H11V17H9V9Z" fill="currentColor" />
            <path d="M13 9H15V17H13V9Z" fill="currentColor" />
          </svg></button></h2>
        `;
        list.items.forEach((i, index) => {
            html += `
            <li data-list='${id}' data-index='${index}' class='list__item ${i.Ticked ? 'ticked' : ''}'>
                ${i.Label}
            </li>
            `;
        });
        html += `
            <div class='list__control'>
                <input id='${id}Input' type='text'  />
                <button id='${id}Button' data-list='${id}' class='add-item'>Add Item</button>
            </div>
        `;
        html += `</ul>`;
    });
    listHolder.innerHTML = html;
    updateListeners();
}

function update() {
    Object.entries(lists).forEach(l => {
        if (l[1] == null) {
            db.collection('lists').doc(l[0]).delete();
        } else {
            db.collection('lists').doc(l[0]).set(l[1]);
        }
    });
}

function listenForUpdates() {
    db.collection('lists').onSnapshot(s => {
        lists = {};
        s.docs.forEach(d => {
            lists[d.id] = d.data();            
        });
        renderLists();
    }, e => {
        console.error(e);
    });
}

function addList() {
    lists[newListInput.value.replace(/\s+/g, '').replace(/[:,\.\-_\*&^%$£@!#\+=\/\\\?~`;±§]+/g, '')] = {
        items: [],
        title: newListInput.value
    };
    update();
}

function init() {
    firebase.initializeApp(firebaseConfig);

    db = firebase.firestore();
    listenForUpdates();

    addListButton.addEventListener('click', addList, false);
}

export {
    init
}