import {loadSearch} from './_search';

var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;

let recognition;

function receiveSpeech(event) {
    // document.body.classList.remove('stop');
    let words = event.results[0][0].transcript;
    search.value = '';
    search.value = words;
}

function startRecognition() {
    recognition.start();
}

function stopRecognition() {
    recognition.stop();
}

function init() {
    recognition = new SpeechRecognition();

    recognition.continuous = true;
    recognition.lang = 'en-GB';
    recognition.interimResults = true;
    recognition.maxAlternatives = 1;

    recognition.addEventListener('speechend', stopRecognition, false);

    search.addEventListener('focus', startRecognition, false);
    search.addEventListener('blur', stopRecognition, false);

    recognition.addEventListener('result', receiveSpeech, false);

}

export {
    init
}