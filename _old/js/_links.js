let linksArray = [];
let localLinks = [];

class Link {
    constructor(element, link, id) {
        this.el = element;
        this.link = link;
        this.id = id;

        let deleteBtn = this.el.querySelector('.delete-item');
        deleteBtn.addEventListener('click', this.delete, false);
    }

    delete() {
        let links = JSON.parse(localStorage.getItem('savedLinks'));
        links.splice(this.id, 1);
        localStorage.setItem('savedLinks', JSON.stringify(links));
        fetchLinks();
    }
}

function addLink() {
    let dialog = document.querySelector('.dialog-links');
    let links = JSON.parse(localStorage.getItem('savedLinks'));
    if (!links) {
        links = [];
    }
    let newLink = {
        url: '',
        icon: '',
        name: ''
    };
    if (!linkUrl.value) {
        linkUrl.invalid;
        return;
    }
    if (!linkName.value) {
        linkName.invalid;
        return;
    }
    if (!linkIcon.value) {
        linkIcon.invalid;
        return;
    }
    newLink.url = linkUrl.value;
    newLink.name = linkName.value;
    newLink.icon = linkIcon.value;

    linkUrl.value = null;
    linkName.value = null;
    linkIcon.value = null;

    dialog.hide();

    links.push(newLink);

    localStorage.setItem('savedLinks', JSON.stringify(links));

    fetchLinks();
}

function toggleEdit() {
    linksEl.classList.toggle('editing');
}

function fetchLinks() {
    mouseMover();
    localLinks = JSON.parse(localStorage.getItem('savedLinks'));
    if (!localLinks) {
        return;
    }
    linksEl.innerHTML = '';
    linksArray = [];
    localLinks.forEach(link => {
        let listItem = document.createElement('li');
        listItem.innerHTML = `
            <sl-tooltip content='${link.name}'>
                <a href='${link.url}' target='_blank'>
                    <i class='${link.icon}'></i>
                </a>
                <button class='delete-item'><sl-icon name='x'></sl-icon></button>
            </sl-tooltip>`;
        linksEl.appendChild(listItem);
        linksArray.push(new Link(listItem, link));
    });

    editLinks.addEventListener('click', toggleEdit, false);
}

function linksMouseOver(e) {
    let boxPos = linksEl.getBoundingClientRect();
    let mouse = {
        x: e.pageX - boxPos.left,
        y: e.pageY - boxPos.top
    };
    let center = {
        x: linksEl.clientWidth / 2,
        y: linksEl.clientHeight / 2
    };
    linksEl.style.setProperty('--mouse-x', mouse.x - center.x);
    linksEl.style.setProperty('--mouse-y', mouse.y - center.y);
}

function mouseMover() {
    linksEl.addEventListener('mousemove', linksMouseOver, false);
}

function initDialog() {
    let dialog = document.querySelector('.dialog-links');
    showAddDialog.addEventListener('click', () => {
        dialog.show();
    }, false);

    addLinkBtn.addEventListener('click', addLink, false);
}

export {
    initDialog,
    fetchLinks
}