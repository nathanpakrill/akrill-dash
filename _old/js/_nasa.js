const key = 'J35GAhUhaNPYzrLecJqEhlMNdgeuZezg4rbTz6mG';

function request() {
    fetch(`https://api.nasa.gov/planetary/apod?api_key=${key}`).then(r => r.json()).then(data => {
        space.src = data.url;
    });

    document.body.addEventListener('mousemove', setTransform, false);
}

function setTransform(e) {
    space.style.setProperty('--translate-x', `${((e.clientX / window.innerWidth) - 0.5) * (space.offsetWidth * 0.15)}px`);
    space.style.setProperty('--translate-y', `${((e.clientY / window.innerHeight) - 0.5) * (space.offsetHeight * 0.15)}px`);
}

export {
    request
}