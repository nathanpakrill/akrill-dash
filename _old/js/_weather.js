const apiKey = '0947e6548cabb95c1033689ce2fc4445';

async function init() {
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=Bournemouth&appid=${apiKey}&units=metric`).then(r => r.json()).then(data => {
        let weather = data;
        temp.innerHTML = `${weather.main.temp}, <small>${weather.main.feels_like}</small>`;
        weatherBox.innerHTML = `${weather.weather[0].description}`;
    }).catch(e => console.error(e));
}

export {
    init
}