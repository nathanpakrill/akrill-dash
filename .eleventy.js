module.exports = function(eleventyConfig) {

    eleventyConfig.addPassthroughCopy('dist');
    eleventyConfig.addPassthroughCopy('img');
    eleventyConfig.addPassthroughCopy('planet');
    eleventyConfig.setUseGitIgnore(false);

    return {
        passthroughFileCopy: true
    }
}